import appdaemon.plugins.hass.hassapi as hass
import time

#
# Announce App
#
# Args:
#		text - text to announce

class announce(hass.Hass):

	def initialize(self):
		self.log("Intializing Project Announce")
		self.listen_state(self.device1_state_callback, entity = self.args["device1"])

	def device1_state_callback(self, entity, attribute, old, new, kwarg):
		self.log("Device " + entity + " state changed from " + old + " to " + new)
		if new == "home":
			self.announce(self.args["text"], self.args["player"])

	def announce(self, text, player):
		announce = self.get_state("input_boolean.announce")
		if announce == "off":
			self.log("Announcments are OFF")
			return
		self.log("Announcing text " + text + " on " + player)
		volume = self.get_state(player, attribute="volume_level")
		self.call_service("media_player/volume_set", entity_id = player, volume_level = 0.6)
		self.call_service("tts/google_say", entity_id = player, message = text)
		self.call_service("media_player/volume_set", entity_id = player, volume_level = volume)
